package walkInGraph;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Driver {
	
	public static Scanner in = new Scanner(System.in);
	public static Graph graph;

	public static void main(String[] args) {

		
		credentials();
		initialiseDubGraph();
		mainMenu();
		
	}
	
	public static void mainMenu() {
		boolean exit = false;
		int option = 0;
		menuDisplay();
		while(!exit) {
			
			try {
				option = in.nextInt();
			} catch (Exception e) {
				System.out.println("Please enter item from menu options");
				in.nextLine();
			}
			
			if(option == 8) {
				exit = true;
			} else if (option >= 1 && option <= 7) {
				switch (option) {
				case 1:
					System.out.println("Enter site name to find :");
					String siteToFind = in.next();
					if(graph.findSite(siteToFind) != -1) {
						System.out.println("Site Exists");
						break;
					} 
					System.out.println("Site Does Not Exists");
					break;
				case 2:
					System.out.println("Enter Location One : ");
					String siteOne = in.next();
					System.out.println("Enter Location two : ");
					String siteTwo = in.next();
					int no1 = graph.findSite(siteOne);
					int no2 = graph.findSite(siteTwo);
					if(no1 == -1 || no2 == -1) {
						System.out.println("Double check if you entered location names corectly");
						break;
					} else if(graph.hasEdge(no1, no2)) {
						System.out.println("There is a path between " + siteOne + " and " + siteTwo);
						break;
					}
					System.out.println("There is no path between " + siteOne + " and " + siteTwo);
					break;
				case 3:
					System.out.println("Enter Location Name to Find Its Adjacent Sites : ");
					String location = in.next();
					int index = graph.findSite(location);
					if(index == -1) {
						System.out.println("Sorry We Couldnt Find Location You Entered");
						break;
					}
					graph.displayAdjSites(index);
					break;
				case 4: 
					System.out.println("Enter Location Name to Find Closest Site To It : ");
					String site = in.next();
					int indexOfSite = graph.findSite(site);
					if(indexOfSite == -1) {
						System.out.println("Sorry We Couldnt Find Location You Entered");
						break;
					}
					graph.findClosestSite(indexOfSite);
					break;
				case 5:
					graph.printEdges();
					break;
				case 6: 
					menuDisplay();
					break;
				case 7:
					graph.printAdjacencyMatrix();
					break;
				default:
					break;
				}
			}
			System.out.print("---------------------------------------------------------------------\nOption : ");
			
		}
	}
	
	public static void menuDisplay() {
		String menu = 
				"1.\tSearch For a Location"+
				"\n2.\tSearch Site-Site"+
				"\n3.\tDisplay Adjacent Sites To Given Site"+
				"\n4.\tDisplay Closest Site To Given Site"+
				"\n5.\tPrint All Locations and Adjacent Locations to Every Location"+ 
				"\n6.\tPrint Menu Options"+
				"\n7.\tPrint Adjacency Matrix"+
				"\n8.\tEXIT"+
				"\nOption : ";
		System.out.print(menu);
	}
	
	public static void initialiseDubGraph() {
		graph = new Graph(9, false, true);
		String st;
		File file = new File("dubLocations.txt");
		String[] arrStrings = new String[5];
		try(BufferedReader br = new BufferedReader(new FileReader(file))) {
			while((st = br.readLine()) != null) {
				arrStrings = st.split(",");
				graph.addEdge(
						Integer.parseInt(arrStrings[0]),
						Integer.parseInt(arrStrings[1]),
						Integer.parseInt(arrStrings[2]),
						arrStrings[3],
						arrStrings[4]);
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e2) {
			System.out.println(e2.getMessage());
		} catch (Exception e3) {
			System.out.println(e3.getMessage());
		}
	}

	public static void credentials() {
		String display = "\tComputing 2: 2019 - 2020"
				+ "\n\tDSA 1 - Assignment 2"
				+ "\n\tProgrammer: Erlandas Bacauskas, Student No: C00242521"
				+ "\n\tprogrammer: Krzysztof Bac, Student No: C00238768"
				+ "\n\nWalk in a Park Application"
				+ "\nUsed Dublin Locations\n";
		System.out.println(display);
	}
}
