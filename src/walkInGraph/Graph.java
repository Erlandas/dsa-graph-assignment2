package walkInGraph;

public class Graph {
	
	private int numOfNodes;			//Holds amount of nodes present
	private boolean directed;		//Specifies if graph is directed
	private boolean weighted;		//Specifies if graph is weighted
	private float[][] matrix;
	private String[] nodes;
	/* Below
	 * For ease of use this boolean 2D array will
	 * let us know if edge exists without checking number values
	 * such as 0
	 */
	private boolean[][] isSetMatrix;
	
	//Constructor that initialises our Graph
	public Graph(int numOfNodes, boolean directed, boolean weighted) {
		this.directed = directed;
		this.weighted = weighted;
		this.numOfNodes = numOfNodes;
		nodes = new String[numOfNodes];
		//Initialise adjacency matrix to the appropriate size
		matrix = new float[numOfNodes][numOfNodes];
		isSetMatrix = new boolean[numOfNodes][numOfNodes];
	}

	/***
	 * Adds an edge on a adjacency matrix
	 * method used if graph is unweighed, it passes default value of 1
	 * @param coordinateX	-	X location in adjacency matrix
	 * @param coordinateY	-	Y location in adjacency matrix
	 * @param nodes	-	location name
	 */
	public void addEdge(int coordinateX, int coordinateY, String nodeX, String nodeY) {
		addEdge(coordinateX, coordinateY, 1, nodeX,nodeY);
	}
	/***
	 * Adds an edge in adjacency matrix
	 * Checks if graph is weighted and if it has correct value
	 * Also if graph is NOT directed it adds values to corresponding locations same as [X][Y] == [Y][X]
	 * @param coordinateX	-	X location in adjacency matrix
	 * @param coordinateY	-	Y location in adjacency matrix
	 * @param nodes	-	location name
	 * @param weight		-	holds value that represents weight on edge
	 */
	public void addEdge(int coordinateX, int coordinateY, float weight, String nodeX, String nodeY) {
		float valueToAdd = weight;
		if(!weighted && valueToAdd != 1) {
			valueToAdd = 1;
		}
		if (nodes[coordinateX] == null) {
			nodes[coordinateX] = nodeX;
		}
		if (nodes[coordinateY] == null) {
			nodes[coordinateY] = nodeY;
		}
		matrix[coordinateX][coordinateY] = valueToAdd;
		isSetMatrix[coordinateX][coordinateY] = true;
		if(!directed) {
			matrix[coordinateY][coordinateX] = valueToAdd;
			isSetMatrix[coordinateY][coordinateX] = true;
		}
	}
	/***
	 * Method that prints adjacency matrix
	 * String format used to print contents within 12 space positions
	 */
	public void printAdjacencyMatrix() {
		//Loop that prints headers for each column
		for (int i = -1; i < numOfNodes; i++) {
			if(i == -1) {
				System.out.format("%12s", "   ");
			}else {
				System.out.format("%12s", nodes[i]);
			}
		}
		System.out.println();
		//Loop that prints values of each vertex also as header for row
		for (int row = 0; row < numOfNodes; row++) {
			System.out.format("%12s", nodes[row]);
			for (int col = 0; col < numOfNodes; col++) {
				
				//We only want to print the values of those positions that have been marked as set
				if(isSetMatrix[row][col]) {
					System.out.format("%12s", String.valueOf(matrix[row][col]));
				} else {
					System.out.format("%12s", "0  ");
				}
			}
			System.out.println();
		}
	}
	/***
	 * Prints all the connected edges within graph
	 */
	public void printEdges() {
	
		for (int row = 0; row < isSetMatrix.length; row++) {
			System.out.print("Location '" + nodes[row] + "' is connected to: ");
			for (int col = 0; col < isSetMatrix.length; col++) {
				if(isSetMatrix[row][col]) {
					System.out.print(nodes[col] + " ");
				}
			}
			System.out.println();
		}
	}
	/***
	 * Returns boolean value indicating if edge exists between given vertices
	 * @param coordinateX	-	X location in adjacency matrix
	 * @param coordinateY	-	Y location in adjacency matrix
	 * @return boolean value true or false if edge exists
	 */
	public boolean hasEdge(int coordinateX, int coordinateY) {
		return isSetMatrix[coordinateX][coordinateY];
	}
	/***
	 * 
	 * @param siteName	-	accepts String as site name
	 * @return	integer indicating location in array of site names or -1 if site is not present
	 */
	public int findSite(String siteName) {
		for (int i = 0; i < numOfNodes; i++) {
			if(siteName.equals(nodes[i])) {
				return i;
			}
		}
		return -1;
	}
	/***
	 * Prints all the adjacent locations to a given site
	 * @param siteNumber	-	integer that indicates site location in an array of names
	 */
	public void displayAdjSites(int siteNumber) {
		System.out.print("Adjacent sites for " + nodes[siteNumber] + " are: ");
		for (int i = 0; i < numOfNodes; i++) {
			if(isSetMatrix[i][siteNumber]) {
				System.out.print(nodes[i] + " ");
			}
		}
		System.out.println();
	}
	/***
	 * Prints closest site to a given site
	 * @param siteNumber	-	integer that indicates site location in an array of names
	 */
	public void findClosestSite(int siteNumber) {
		int closest = 0;
		float weightOfClosest = Integer.MAX_VALUE;
		for (int i = 0; i < numOfNodes; i++) {
			if(matrix[siteNumber][i] > 0) {
				if(matrix[siteNumber][i]<weightOfClosest) {
					closest = i;
					weightOfClosest = matrix[siteNumber][i];
				}
			}
		}
		System.out.println("Closest site is " + nodes[closest] + " with distance of " + weightOfClosest);
	}
 }
